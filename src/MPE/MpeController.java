package MPE;

import javafx.application.Platform;

public class MpeController {
	protected MpeView view;
	protected MpeModel model;

	protected MpeController(MpeView view, MpeModel model) {
		this.view = view;
		this.model = model;

		// register ourselves to listen for property changes in the model.
		// Each change results in a short animation.
		model.getValueProperty().addListener((observable, oldValue, newValue) -> {
			String newText = Integer.toString(model.getValue());
			// Move to the JavaFX thread
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					view.label.setText(newText);
					view.doAnimate();
				}
			});
		});
		
		// register ourselves to listen for button clicks
		view.button.setOnAction((event) -> {
		model.incrementValue();
		String newText = Integer.toString(model.getValue());
		view.label.setText(newText);
		});
		
		// register ourselves to handle window-closing event
		view.getStage().setOnCloseRequest((event) -> {
		view.stop();
		model.stop(); // End the simulation
		});
	}

}
