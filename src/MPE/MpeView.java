package MPE;

import javafx.animation.RotateTransition;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class MpeView {
	protected Stage stage;
	protected MpeModel model;
	
	protected Label label;
	protected Button button;
	
	private RotateTransition rotate;

	protected MpeView(Stage stage, MpeModel model) {
		this.stage=stage;
		this.model=model;
		
		stage.setTitle("Rotate");
		
		GridPane root = new GridPane();
		this.label= new Label();
		this.label.setText(Integer.toString(model.getValue()));
		root.add(this.label,0,0);
		
		this.button= new Button();
		this.button.setText("Click me");
		root.add(this.button, 0, 1);
		
		// Prepare animation for use
		rotate = new RotateTransition(Duration.millis(500));
		rotate.setByAngle(360);
		rotate.setCycleCount(1);
		rotate.setNode(this.label);
		
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("MPE.css").toExternalForm());
		stage.setScene(scene);
		
	}
	
	public void start() {
		stage.show();
	}
	
	public void stop() {
		stage.hide();
		Platform.exit();
	}
	
	//Getter for the stage, so the controller can access Window events
	public Stage getStage() {
		return this.stage;
	}
	
	//Method to allow the Controller to trigger an animation
	public void doAnimate() {
		rotate.play();
	}
	

}
