package MPE;

import javafx.beans.property.SimpleIntegerProperty;

public class MpeModel {
	// final promises that this object will not change
	private final SimpleIntegerProperty value;
	private Simulator sim;

	protected MpeModel() {
		value = new SimpleIntegerProperty();
		value.setValue(0);
		sim = new Simulator();
		sim.start();
	}
	
	public SimpleIntegerProperty getValueProperty() {
		return value;
	}

	public int getValue() {
		return value.get();
	}

	public int incrementValue() {
		int val = value.get();
		val++;
		value.setValue(val);
		return val;
	}
	
	/**
	 * Called, when we should stop our simulation
	 */
	public void stop() {
		if (sim != null) sim.interrupt();
	}

	private class Simulator extends Thread {
		@Override
		public void run() {
			try {
				while (true) {
					int rand = (int) (Math.random() * 5);
					if (rand == 1) // 20% chance of increment
						value.setValue(value.get()+1);
					else if (rand == 2)// 20% chance of decrement
						value.setValue(value.get()-1);
					Thread.sleep(1000);
				}
			} catch (InterruptedException e) {
			}
		}
	}
}
