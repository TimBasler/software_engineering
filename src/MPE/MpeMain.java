package MPE;

import javafx.application.Application;
import javafx.stage.Stage;

public class MpeMain extends Application {
	private MpeModel model;
	private MpeView view;
	private MpeController controller;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//Initialize the GUI
		this.model= new MpeModel();
		this.view= new MpeView(primaryStage, model);
		this.controller= new MpeController(view,model);
		//Displays the GUI after all initial is finish
		this.view.start();
	}
	
	public void stop() {
		if(view!=null) {
			view.stop();
		}
	}

}
