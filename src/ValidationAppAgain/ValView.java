package ValidationAppAgain;

import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ValView {
	protected ValModel model;
	protected Stage stage;
	
	protected TextField text;
	
	protected ValView(Stage stage, ValModel model) {
		this.stage=stage;
		this.model=model;
		
		stage.setTitle("Validation");
		
		GridPane root = new GridPane();
		this.text = new TextField();
		root.add(text, 0,0 );
		
		Scene scene = new Scene (root);
		stage.setScene(scene);
		
	}
	public void start() {
		stage.show();
	}
	
	public void stop() {
		stage.hide();
	}
	
	 /**
     * Getter for the stage, so that the controller can access window events
     */
	public Stage getStage() {
		return stage;
	}
	
	

}
