package ValidationAppAgain;

public class ValController {
	protected ValView view;
	protected ValModel model;

	protected ValController(ValView view, ValModel model) {
		this.view = view;
		this.model = model;

		view.text.textProperty().addListener(
				// Parameters of any PropertyChangeListener
				(observable, oldValue, newValue) -> validateAdress(newValue));
	}

	private void validateAdress(String newValue) {
		boolean valid = false;

		if (newValue.charAt(newValue.length() - 1) != '@') {
			String[] adressParts = newValue.split("@");
			if (adressParts.length == 2 && !adressParts[0].isEmpty()) {
				String[] domainParts = adressParts[1].split("\\.");
				if (domainParts.length >= 2) {
					valid = true;
					for (String s : domainParts) {
						if (s.length() < 2) {
							valid = false;
							break;
						}
					}
				}
			}
		}

		if (valid) {
			view.text.setStyle("-fx-text-inner-color: green;");
		} else {
			view.text.setStyle("-fx-text-inner-color: red;");
		}
		
	}

}
