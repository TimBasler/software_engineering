package ValidationAppAgain;

import javafx.application.Application;
import javafx.stage.Stage;

public class ValApp extends Application {
	protected ValView view;
	protected ValController controller;
	protected ValModel model;
	
	 @Override
	public void start(Stage primaryStage) throws Exception {
		this.model= new ValModel();
		this.view= new ValView(primaryStage, model);
		this.controller = new ValController(view,model);
	
	
		this.view.start();
	}
	
	public static void main(String[]args) {
		launch(args);
	}

}
