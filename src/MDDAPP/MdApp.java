package MDDAPP;

import javafx.application.Application;
import javafx.stage.Stage;

public class MdApp extends Application  {
	private MdView view;
	private MdController controller;
	private MdModel model;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//Initialize the GUI
		this.view= new MdView(primaryStage, model);
		this.controller = new MdController(model,view);
		this.model= new MdModel();
		//Displays the GUI after all initialition is complete
		view.start();
	}

}
