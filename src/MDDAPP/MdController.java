package MDDAPP;

import javafx.stage.Stage;

public class MdController {
	protected MdModel model;
	protected MdView view;
	
	protected MdController(MdModel model, MdView view) {
		this.model=model;
		this.view= view;
		
		this.view.label.setOnMouseEntered((mouseEvent)->Woof("Mouse detected"));
		this.view.label.setOnMouseExited((mouseEvent)->Woof("Mouse lost"));
		this.view.label.setOnMouseClicked((mouseEvent)->Woof("Mouse Clicked"));
		this.view.label.setOnMouseReleased((mouseEvent)->Woof("Mouse Released"));
	}
	
	private void Woof(String in) {
		this.view.label.setText(in);
	}

}
