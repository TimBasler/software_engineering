package MDDAPP;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MdView {
	protected Stage stage;
	protected MdModel model;
	
	protected Label label;
	
	protected MdView(Stage stage,MdModel model) {
		this.stage=stage;
		this.model= model;
		
		stage.setTitle("MouseDetector");
		
		GridPane root = new GridPane();
		this.label= new Label("This is a Label");
		label.setMinSize(200, 200);
		root.add(label, 0, 0);
	
		Scene scene = new Scene (root);
		stage.setScene(scene);
	}
	
	public void start() {
		stage.show();
	}
}
