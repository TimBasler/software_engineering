package FirstAnimation;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class BouncingRectangle extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// Create new object with initial position and size
		Rectangle rect = new Rectangle(50, 50);
		rect.setStroke(Color.BLACK);
		rect.setStrokeWidth(5);
		rect.setFill(Color.GREEN);

		// Create a group to hold our animated objects
		Group objects = new Group();
		objects.getChildren().add(rect);

		// Timeline Animation
		Timeline time = new Timeline();
		time.setCycleCount(Animation.INDEFINITE);

		// 1Position
		KeyValue xPos = new KeyValue(rect.translateXProperty(), 350);
		KeyValue yPos = new KeyValue(rect.translateYProperty(), 0);
		KeyValue fill = new KeyValue(rect.fillProperty(), Color.GREEN);
		KeyFrame keyFrame = new KeyFrame(Duration.seconds(1.0), xPos, yPos, fill);
		time.getKeyFrames().add(keyFrame);

		// 2Position
		KeyValue xPos2 = new KeyValue(rect.translateXProperty(), 350);
		KeyValue yPos2 = new KeyValue(rect.translateYProperty(), 350);
		fill = new KeyValue(rect.fillProperty(), Color.BROWN);
		keyFrame = new KeyFrame(Duration.seconds(2.0), xPos2, yPos2, fill);
		time.getKeyFrames().add(keyFrame);

		// 3Position
		KeyValue xPos3 = new KeyValue(rect.translateXProperty(), 0);
		KeyValue yPos3 = new KeyValue(rect.translateYProperty(), 350);
		fill = new KeyValue(rect.fillProperty(), Color.RED);
		keyFrame = new KeyFrame(Duration.seconds(3.0), yPos3, xPos3, fill);
		time.getKeyFrames().add(keyFrame);

		// 4Position
		KeyValue xPos4 = new KeyValue(rect.translateXProperty(), 0);
		KeyValue yPos4 = new KeyValue(rect.translateYProperty(), 0);
		fill = new KeyValue(rect.fillProperty(), Color.BLUE);
		keyFrame = new KeyFrame(Duration.seconds(4.0), yPos4, xPos4, fill);
		time.getKeyFrames().add(keyFrame);

		// 5Position
		KeyValue xPos5 = new KeyValue(rect.translateXProperty(), 350);
		KeyValue yPos5 = new KeyValue(rect.translateYProperty(), 0);
		fill = new KeyValue(rect.fillProperty(), Color.GREEN);
		keyFrame = new KeyFrame(Duration.seconds(1.0), xPos5, yPos5, fill);
		time.getKeyFrames().add(keyFrame);

		time.play();

		// Display the scene, which contains our object-group
		Scene scene = new Scene(objects, 400, 400, Color.WHITE);
		primaryStage.setTitle("AnimationTest");
		primaryStage.setScene(scene);
		primaryStage.show();

	}

}
