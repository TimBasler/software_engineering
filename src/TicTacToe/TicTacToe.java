package TicTacToe;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class TicTacToe extends Application {

	public static void main(String[] args)  {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		GridPane grid = new GridPane();
		
		//Create the buttons : 3 columns 3 rows
		for(int col = 0 ; col< 3 ; col ++) {
			for(int row = 0 ; row <3; row ++) {
				//Create button for this position
				Button btn = new Button();
				grid.add(btn, col, row);
				
				//Set button size
				btn.setPrefSize(120, 120);
				
				//What happens when clicked ? 
				btn.setOnAction(this::setButtonText);//TODO Method doesn t exist
				
			}
		}
		
		//Create the Scene using our Layout ; then display it ! 
		Scene scene = new Scene (grid);
		scene.getStylesheets().add(getClass().getResource("TicTacToe.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setTitle("TicTacToe");
		primaryStage.show();
	}

	//when a button is clicked 
	enum ValidMoves{X,O};//possible Moves
	private ValidMoves nextMove = ValidMoves.X;
	
	private void setButtonText(ActionEvent e) {
		Button btn =  (Button) e.getSource();
		btn.setText(nextMove.toString());
		if(nextMove.equals(ValidMoves.X)) {
			nextMove = ValidMoves.O;
		}else {
			nextMove= ValidMoves.X;
			
		}
	}
	
	
	
}
